

describe('notes module',function(){
    
    beforeEach(function(){
        notes.clear();
        notes.add('1');
        notes.add('2');
        notes.add('3');
        notes.add('4');
        notes.add('5');
        
    });
   it("should be able to add new a new node",function(){
    expect(notes.add('sixth note')).toBe(true);
    expect(notes.count()).toBe(6);
  });

    it('should ignore blank note',function() {
        expect(notes.add('')).toBe(false);
        expect(notes.count()).toBe(5);
        pending();
        
    });
    it('should ignore notes contaning only whitespace',function() {
        expect(notes.add('  ')).toBe(false);
        expect(notes.count()).toBe(5);
        pending();
       
    });
    it('should require a string parameter',function() {
        expect(notes.add()).toBe(false);
        expect(notes.count()).toBe(5);
        pending();
    });
    
    describe('adding a note',function() {
        
    })
    
    describe('deleteing a note',function() {
        it('should be able to delete the first index');
        it('should be able to delete the last index');
        it('should return false if index is out of range');
        it('should return fasle if the parameter is missing');
    })
});


